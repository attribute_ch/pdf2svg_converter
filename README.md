# PDF to SVG conversion Workflow

## Installation

1. open your terminal foun in `/Applications/Utilities` and install homebrew
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

2. install pdf2svg
```
brew install pdf2svg
```

3. download and install [node.js](https://nodejs.org/en/download/)  after that install svgo via npm
```
npm install -g svgo
```

## Usage

run:

```
bash myscript.sh
```

## to consider...
To ensure image quality and consistency throughout the page you need to take care of following constraints:
* image format of X Width and Y Height.
* a Stroke width of  Z pixels
* hatching bottom left to top right of 5 pixels
* place the image without whitespace, scaling is taken care of by the site.
