#!/bin/bash
for infile in *.[Pp][Dd][Ff];
do
  echo "Processing $infile...";
  outFilename=$(echo $infile | cut -f 1 -d '.')
  pdf2svg $infile $outFilename.svg
  svgo -p 3 $outFilename.svg $outFilename.min.svg
done
